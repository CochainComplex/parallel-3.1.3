/*

Copyright (C) 2009 VZLU Prague

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, see <http://www.gnu.org/licenses/>.

*/

// Author: Jaroslav Hajek <highegg@gmail.com>

#include <octave/oct.h>
#include <octave/oct-stream.h>

#include "minimal-load-save.h"

DEFUN_DLD (fsave, args, ,
  "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} fsave (@var{fid}, @var{var})\n\
Save a single variable to a binary stream, to be subsequently loaded with\n\
fload. Returns true if successful.\n\
Not suitable for data transfer between machines of different type.\n\
@end deftypefn")
{
  octave_value retval;
  int nargin = args.length ();

  if (nargin == 2)
    {
      int fid = OCTAVE__INTERPRETER__STREAM_LIST__GET_FILE_NUMBER (args(0));

      OCTAVE__STREAM octs = OCTAVE__INTERPRETER__STREAM_LIST__LOOKUP
        (fid, "fsave");
      std::ostream *os = octs.output_stream ();

      octave_value val = args(1);

      if (os)
        {
          minimal_write_header (*os);

          if (minimal_write_data (*os, val))
            error ("fsave: could not save variable.");
        }
      else
        error ("fsave: stream not opened for writing.");
    }
  else
    print_usage ();

  return retval;
}
