// Olaf Till <i7tiol@t-online.de>
// This file is granted to the public domain.

#ifdef HAVE_OCTAVE_INTERPRETER_H
// >= octave-4.2
#include <octave/octave.h>
#include <octave/interpreter.h>
#include <octave/call-stack.h>
#else
// <= octave-4.0..
#include <octave/toplev.h>
#endif
