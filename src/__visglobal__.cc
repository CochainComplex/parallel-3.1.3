// Copyright (C) 2015-2018 Olaf Till <i7tiol@t-online.de>

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; If not, see <http://www.gnu.org/licenses/>.

#include <octave/oct.h>

#include "config.h"

#include <octave/pt-all.h>

#include "error-helpers.h"

DEFUN_DLD (__visglobal__, args, ,
           "-*- texinfo -*-\n\
@deftypefn {Loadable Function} __visglobal__ (@var{vars})\n\
Internal function. Vector version of isglobal(), executed in calling frame. @var{vars} is a cellstr.\n\
@end deftypefn")
{
  std::string fname ("__visglobal__");

  octave_value err_retval;

  if (args.length () != 1)
    {
      print_usage ();

      return octave_value_list ();
    }

  bool err;

  Array<std::string> vars;

  SET_ERR (vars = args(0).cellstr_value (), err);

  if (err)
    {
      print_usage ();

      return octave_value_list ();
    }

  octave_idx_type n = vars.numel ();

  boolNDArray retval (dim_vector (n, 1));

  OCTAVE__UNWIND_PROTECT frame;

  frame.ADD_OCTAVE__INTERPRETER__CALL_STACK__POP;

  CHECK_ERROR (OCTAVE__INTERPRETER__CALL_STACK__GOTO_CALLER_FRAME (),
               err_retval,
               "%s: goto_caller_frame() returned an error", fname.c_str ());

  for (octave_idx_type i = 0; i < n; i++)
    retval(i) = OCTAVE__INTERPRETER__CURRENT_SCOPE__IS_GLOBAL (vars(i));

  return octave_value (retval);
}
