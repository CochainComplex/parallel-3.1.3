/*

Copyright (C) 2015-2018 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __OCT_PARALLEL_GNUTLS__

#define __OCT_PARALLEL_GNUTLS__

// to have Octaves config.h included before config.h of 'parallel'
#include <octave/oct.h>

#include "config.h"

#ifdef HAVE_LIBGNUTLS
#include <gnutls/gnutls.h>
#endif

#include <sys/stat.h>

#include <sys/types.h>

#include <fcntl.h>

#include <sys/mman.h>

#include <errno.h>

#include <sys/wait.h>

#include <stdio.h>

#ifdef HAVE_MALLOC_H
#include <malloc.h>
#elif HAVE_SYS_MALLOC_H
#include <sys/malloc.h>
#else
#error "No malloc.h present."
#endif

/*
not necessary with current Octave
#include <stdlib.h>
#include <string.h>
*/

#include <unistd.h>
// next is from gnulib, getpass of unistd.h is deprecated
/* Get getpass declaration, if available.  */
// # if defined HAVE_DECL_GETPASS && !HAVE_DECL_GETPASS
#ifndef HAVE_GETPASS
/* Read a password of arbitrary length from /dev/tty or stdin.  */
extern "C" {
char *getpass (const char *prompt);
}
#endif

// We link against the gnulib num_processors() used by Octave. nproc.h
// used by Octave is not accessible. If the interface changes, this
// will stop working.
extern "C" {
enum nproc_query
{
  NPROC_ALL,                 /* total number of processors */
  NPROC_CURRENT,             /* processors available to the current process */
  NPROC_CURRENT_OVERRIDABLE  /* likewise, but overridable through the
                                OMP_NUM_THREADS environment variable */
};

/* Return the total number of processors.  The result is guaranteed to
   be at least 1.  */
extern unsigned long int num_processors (enum nproc_query query);
}


// Octave includes

#include <octave/oct.h>

#include <octave/file-stat.h>

#include <octave/file-ops.h>

#include <octave/parse.h>

#include <octave/ov-struct.h>


#define N_CONNECT_RETRIES 10

/* define some of the following for debugging, but take care:
   sensitive data (e.g. the password) will be written to standard
   error and to logfiles */
#undef octave_parallel_debug_server
// #define octave_parallel_debug_server 1
#undef octave_parallel_debug_client
// #define octave_parallel_debug_client 1
#undef octave_parallel_debug_lib
// #define octave_parallel_debug_lib 1

#ifdef octave_parallel_debug_server
  #define dsprintf(...) fprintf (stderr, __VA_ARGS__); fflush (stderr);
#else
  #define dsprintf(...)
#endif

#ifdef octave_parallel_debug_client
  #define dcprintf(...) fprintf (stderr, __VA_ARGS__);
#else
  #define dcprintf(...)
#endif

#ifdef octave_parallel_debug_lib
  #define dlprintf(...) fprintf (stderr, __VA_ARGS__); fflush (stderr);
#else
  #define dlprintf(...)
#endif

// parallel package includes

#include "error-helpers.h"

#ifdef HAVE_LIBGNUTLS
#include "sensitive-data.h"
#endif

#include "p-streams.h"

#include "p-connection.h"

#include "p-sighandler.h"

#include "minimal-load-save.h"

#ifdef HAVE_LIBGNUTLS
void parallel_gnutls_set_mem_functions (void);
#endif

#endif // __OCT_PARALLEL_GNUTLS__
