/*

Copyright (C) 2002 Hayato Fujiwara <h_fujiwara@users.sourceforge.net>
Copyright (C) 2010-2018 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

// PKG_ADD: autoload ("sclose", "parallel_interface.oct");
// PKG_DEL: autoload ("sclose", "parallel_interface.oct", "remove");

#include <octave/oct.h>
#include <octave/oct-stream.h>
#include <octave/dirfns.h>
#include <octave/oct-map.h>
#include <octave/oct-env.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>

#include "parallel-gnutls.h"

DEFUN_DLD (sclose, args, ,
           "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {} sclose (@var{connections})\n\
Close the parallel cluster network to which @var{connections} belongs.\n\
\n\
See @code{pconnect} for a description of the @var{connections}\n\
variable. All connections of the network are closed, even if\n\
@var{connections} contains only a subnet or a single connection.\n\
\n\
@seealso{pconnect, pserver, reval, psend, precv, parallel_generate_srp_data, select_sockets}\n\
@end deftypefn")
{
  std::string fname ("sclose");

  octave_value retval;

  if (args.length () != 1 ||
      args(0).type_id () != octave_parallel_connections::static_type_id ())
    {
      print_usage ();

      return retval;
    }

  const octave_base_value &rep = args(0).get_rep ();
  const octave_parallel_connections &cconns =
    dynamic_cast<const octave_parallel_connections&> (rep);
  octave_parallel_connections_rep *conns = cconns.get_rep ();

  inthandler_dont_restart_syscalls __inthandler_guard__;

  conns->close ();

  return retval;
}


/*
;;; Local Variables: ***
;;; mode: C++ ***
;;; End: ***
*/
